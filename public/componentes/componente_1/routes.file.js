(function(){
    angular
    .module('app')
    .config(function($stateProvider, $urlRouterProvider){
            $urlRouterProvider.otherwise("/acerca")
            $stateProvider
            .state('acerca',{
                url: '/acerca',
                templateUrl: 'componentes/componente_1/file.html',
                controller: 'File1Controller as vm'
            })
        })
})();