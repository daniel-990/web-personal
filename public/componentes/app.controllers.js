(function(){
	'use strict';
	angular
		.module('app')
		.controller('File1Controller', File1Controller)
        .controller('File3Controller', File3Controller);

      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyAA2oobvOeDZUEx-wzj7FJlL4CZIvv0v64",
        authDomain: "danielarango990-29a5e.firebaseapp.com",
        databaseURL: "https://danielarango990-29a5e.firebaseio.com",
        projectId: "danielarango990-29a5e",
        storageBucket: "danielarango990-29a5e.appspot.com",
        messagingSenderId: "887049675040"
      };
      firebase.initializeApp(config);

        var storageRef = firebase.storage().ref();
        var db = firebase.database().ref('blog/');
        var db_2 = firebase.database().ref('pagina/');

     function File1Controller($stateParams, $http){
            var vm = this;

            db_2.on('child_added', function(data){

console.log(data.val().contenido);
                var plantilla_acerca = $("#plantilla_acerca").html();
                var templateAcerca = Handlebars.compile(plantilla_acerca);


                var contexto_acerca = {
                    icono: data.val().icono,
                    titulo_pag: data.val().titulo_pag,
                    contenido: data.val().contenido,
                    titulo_redes: data.val().titulo_redes,
                    gmail: data.val().gmail,
                    behance: data.val().behance,
                    bitbucket: data.val().bitbucket,
                    youtube: data.val().youtube,
                }
                var mostrar_contenido = templateAcerca(contexto_acerca);
                $("#acerca-de").append(mostrar_contenido);
            });

     }

     function File3Controller($stateParams){
			var vm = this;

                db.on('child_added', function(data){

                var plantilla_blog = $("#plantilla_blog").html();
                var templateBlog = Handlebars.compile(plantilla_blog);


                var contexto_blog = {
                    titulo_blog: data.val().titulo_blog,
                    contenido_blog: data.val().contenido_blog,
                    img_blog: data.val().img_blog,
                }
                var mostrar_contenido = templateBlog(contexto_blog);
                $("#blog-info").append(mostrar_contenido);
            });           
     }


})();