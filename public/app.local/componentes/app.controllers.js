(function(){
	'use strict';
	angular
		.module('app')
		.controller('File1Controller', File1Controller)
		.controller('File2Controller', File2Controller);
     

      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyAA2oobvOeDZUEx-wzj7FJlL4CZIvv0v64",
        authDomain: "danielarango990-29a5e.firebaseapp.com",
        databaseURL: "https://danielarango990-29a5e.firebaseio.com",
        projectId: "danielarango990-29a5e",
        storageBucket: "danielarango990-29a5e.appspot.com",
        messagingSenderId: "887049675040"
      };
      firebase.initializeApp(config);

        var storageRef = firebase.storage().ref();
        var db = firebase.database();

        Parse.initialize("DuJFfqKBkLD1J02HMM77Y7YLjHeFV0Lm2yWVCbXa", "RuxZgxXdnLX9SYwBnOQbxoyOEcCNJ0NpEN9D2no8");
        Parse.serverURL = "https://parseapi.back4app.com/";

      function login(){

          firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                console.log(user.email);
                console.log(user.emailVerified);
                console.log(user.isAnonymous);

                $("#nombre").html(user.email);
            } else {
                console.log("no inicio sesion");

                $("input").prop('disabled', true);
                $("textarea").prop('disabled', true);
                $("button").prop('disabled', true);

                var user = prompt("por favor ingrese su usuario");
                var pass = prompt("por favor ingrese su contraseña");

                if (pass == null || pass == "") {
                    
                } else {
                    firebase.auth().onAuthStateChanged(function(user) {
                        if (user) {
                            var email = user.email;
                            console.log(email);
                            location.reload();
                        } else {
                            alert("error!");
                        }
                    });
                    firebase.auth().signInWithEmailAndPassword(user, pass);
                }
            }
        });

      }
     

     function File1Controller($stateParams, $http){
            var vm = this;

            login();
            $("#cerrar-s").on('click', function(event){
                event.preventDefault();
                firebase.auth().signOut().then(function(){
                    alert("cerro sesion");
                }, function(error) {
                  console.log(error);
                });
            });

            var btnIcono =  $("#btn_icono");
            var btnTitulo = $("#btn_titulo");
            var btnContenido = $("#btn_contenido");
            var btnTituloRedes = $("#btn_titulo_redes");
            var btnRedes = $("#btn_redes");


            btnIcono.on('click', function(){
                var icono = $("#icono").val();

                db.ref('/pagina/app').update({
                    'icono':icono,
                },function(){
                    alert ("datos guardados");
                    location.reload();
                });

            });


            btnTitulo.on('click', function(){
                var titulo_pag = $("#titulo_pag").val();

                db.ref('/pagina/app').update({
                    'titulo_pag':titulo_pag,
                },function(){
                    alert ("datos guardados");
                    location.reload();
                });

            });


            btnContenido.on('click', function(){
                var contenido = $("#contenido").val();

                db.ref('/pagina/app').update({
                    'contenido':contenido,
                },function(){
                    alert ("datos guardados");
                    location.reload();
                });

            });


            btnTituloRedes.on('click', function(){
                var tiutlo_redes = $("#tiutlo_redes").val();

                db.ref('/pagina/app').update({
                    'titulo_redes':tiutlo_redes,
                },function(){
                    alert ("datos guardados");
                    location.reload();
                });

            });


            btnRedes.on('click', function(){
                var gmail = $("#gmail").val();
                var behance = $("#behance").val();
                var bitbucket = $("#bitbucket").val();
                var youtube = $("#youtube").val();

                db.ref('/pagina/app').update({
                    'gmail':gmail,
                    'behance':behance,
                    'bitbucket':bitbucket,
                    'youtube':youtube,
                },function(){
                    alert ("datos guardados");
                    location.reload();
                });

            });

     }

     function File2Controller($stateParams){
     		    var vm = this;

            login();

            var btnEnviar = $("#btn_blog");
            var file = $("#file");

            file.on('change',function(){

                var fileUploadControl = $("#file")[0];
                if (fileUploadControl.files.length > 0) {
                  var file = fileUploadControl.files[0];
                  var name = "photo.jpg";
                  var uploader = $("#uploader");

                  var parseFile = new Parse.File(name, file);

                    parseFile.save().then(function(name){
                      var archivoUrl = name._url;
                      $("#img").attr('value',archivoUrl);

                      console.log(name);

                      console.log("imagen cargada");
                    },function(error) {
                      console.log(error);
                    });

                    var jobApplication = new Parse.Object("DanielApp");
                    jobApplication.set("applicantName", "Daniel img app/blog");
                    jobApplication.set("applicantResumeFile", parseFile);
                    jobApplication.save();
                }

            })

            btnEnviar.on('click', function(){
                var tituloBlog = $("#titulo_blog").val();
                var contenidoBlog = $("#contenido_blog").val();
                var img = $("#img").val();

                db.ref('/blog').push({
                    'titulo_blog':tituloBlog,
                    'contenido_blog':contenidoBlog,
                    'img_blog':img
                },function(){
                    alert ("datos guardados");
                    location.reload();
                });

            });



     }

})();
