(function(){
    angular
    .module('app')
    .config(function($stateProvider, $urlRouterProvider){
            $urlRouterProvider.otherwise("/pagina")
            $stateProvider
            .state('pagina',{
                url: '/pagina',
                templateUrl: 'componentes/componente_1/file.html',
                controller: 'File1Controller as vm'
            })
        })
})();